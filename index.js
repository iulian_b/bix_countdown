var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use('/enduser/css', express.static(__dirname + '/enduser/css'));
app.use('/enduser/js', express.static(__dirname + '/enduser/js'));
app.use('/counter', express.static(__dirname + '/counter'));
app.use('/resources', express.static(__dirname + '/resources'));

app.use('/admin/js', express.static(__dirname + '/admin/js'));
app.use('/admin/css', express.static(__dirname + '/admin/css'));

app.get('/', function(req, res){
	res.sendFile(__dirname + '/enduser/index.html');
});

app.get('/admin', function(req, res){
	res.sendFile(__dirname + '/admin/index.html');
});

io.on('connection', function(socket){
	socket.on('new conn', function(msg){
		io.emit('new conn');
	});
	socket.on('init counter', function(msg){
		io.emit('init counter', msg);
	});
	socket.on('reset counter', function(msg){
		io.emit('reset counter', msg);
	});
	socket.on('toggle counter', function(msg){
		io.emit('toggle counter', msg);
	});
	socket.on('send message', function(msg){
		io.emit('send message', msg);
	});
});


http.listen(3000, "counter.noip.me");