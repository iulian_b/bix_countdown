var Counter = {};

function counter_done() {
	$('#defaultCountdown').addClass('counter_done');
	$('#counter_button').removeClass('button_pause');
	$('#counter_button').text('Done');
	Counter.settings.state = 'stopped';
	Counter.music.stop();
	Counter.notif.play();
}
$(document).ready(function() {

	/*----------  Counter  ----------*/
	var CounterClass = function() {
		this.settings = {
			compact: true,
			format: 'MS',
			description: '',
			onExpiry: counter_done,
			action: '',
			state: 'stopped',
		};
		this.counter = {};
	}
	CounterClass.prototype.init = function(elem_id) {
		this.counter = $(elem_id);
		this.counter.countdown(this.settings);
		this.counter.countdown('pause');
	}
	CounterClass.prototype.play = function() {
		this.counter.countdown('resume');
		this.settings.state = 'playing';
		this.music.play();
	}
	CounterClass.prototype.pause = function() {
		this.counter.countdown('pause');
		if (this.settings.state == 'playing') {
			this.settings.state = 'paused';
		}
		this.music.pause();
	}
	CounterClass.prototype.stop = function() {
		this.counter.countdown('pause');
		this.settings.state = 'stopped';
		this.music.stop();
	}
	/*----------  Counter  ----------*/

	/*----------  Audio   ----------*/
	var MyAudio = function() {
		this.track = {};
		this.loop = 0;
		this.audio_state = 'stopped';
	};
	MyAudio.prototype.init = function(audio_id){
		this.track = document.getElementById(audio_id);
		this.track.loop = this.loop;
	};
	MyAudio.prototype.play = function() {
		if (!$.isEmptyObject(this.track)) {
			this.track.play();
			this.audio_state = 'playing';
		}
	}
	MyAudio.prototype.pause = function() {
		if (!$.isEmptyObject(this.track)) {
			this.track.pause();
			if (this.audio_state == 'playing') {
				this.audio_state = 'paused';
			}
		}
	}
	MyAudio.prototype.stop = function() {
		if (!$.isEmptyObject(this.track)) {
			this.track.pause();
			this.track.currentTime = 0;
			this.audio_state = 'stopped';
		}
	}
	MyAudio.prototype.volume = function(level) {
		if (!$.isEmptyObject(this.track)) {
			this.track.volume = level;
		}
	}
	
	Counter = new CounterClass();
	Counter.music = new MyAudio();
	Counter.notif = new MyAudio();
	if ($('#audio_music').length) {
		Counter.music.init('audio_music');
		Counter.music.loop = 1;
	}
	if ($('#audio_notification').length) {
		Counter.notif.init('audio_notification');
		Counter.notif.track.onended = function() {
			if (Counter.settings.state == 'playing') {
				Counter.music.volume(1);
			}
		}
	}
	/*----------  Audio  ----------*/


	/*----------  Socket  ----------*/
	
	Counter.socket = io();
	Counter.socket.on('toggle counter', function(msg){
		var action = msg.action;
		Counter.counter.countdown('destroy');
		msg.onExpiry = counter_done;
		Counter.counter.countdown(msg);
		if (action == 'resume') {
			Counter.play();
		} else if (action == 'pause') {
			Counter.pause();
		}
	});

	Counter.socket.on('reset counter', function(msg){
		Counter.counter.countdown('destroy');
		$('#defaultCountdown').removeClass('counter_done');
		Counter.counter.countdown(msg);
		Counter.stop();
	});

	Counter.socket.on('send message', function(msg){
		if (!$.isEmptyObject(msg)) {
			if (Counter.settings.state == 'playing') {
				Counter.music.volume(0.5);
			}
			Counter.notif.play();
			$('#defaultCountdown').addClass('countdown_with_text');
		} else {
			$('#defaultCountdown').removeClass('countdown_with_text');
		}
		$('#message_here').html(msg);
	});
	/*----------  Socket  ----------*/

});