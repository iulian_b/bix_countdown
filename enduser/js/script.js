$(document).ready(function() {
	Counter.socket.emit('new conn');
	Counter.socket.on('init counter', function(msg){
		Counter.settings = msg;
		Counter.settings.onExpiry = counter_done;
		Counter.init('#defaultCountdown');
		if (msg.state == 'playing') {
			Counter.play();
		} else {
			Counter.pause();
		}
		if (!$.isEmptyObject(msg.msg_text)) {
			$('#message_here').html(msg.msg_text);
			$('#defaultCountdown').addClass('countdown_with_text');
		}
	});
});