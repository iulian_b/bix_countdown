$(document).ready(function() {
	Counter.init('#defaultCountdown');

	Counter.socket.on('new conn', function(msg){
		var time = Counter.counter.countdown('getTimes');
		var until_time = time[5]*60 + time[6];
		Counter.settings.until = until_time;
		Counter.settings.msg_text = $('#message_input').val();
		Counter.socket.emit('init counter', Counter.settings);
	});
	$('#counter_button').on('click', function() {
		if ($(this).hasClass('button_pause')) {
			$(this).removeClass('button_pause');
			$(this).addClass('button_resume');
			$(this).text('Resume');
			Counter.settings.action = 'pause';
		} else if ($(this).hasClass('button_resume')) {
			$(this).removeClass('button_resume');
			$(this).addClass('button_pause');
			$(this).text('Pause');
			Counter.settings.action = 'resume';
		}

		if ($(this).hasClass('start')) {
			Counter.settings.until = $('#time_input input').val();
			$(this).removeClass('start');
		} else {
			var time = Counter.counter.countdown('getTimes');
			var until_time = time[5]*60 + time[6];
			Counter.settings.until = until_time;
		}
		Counter.socket.emit('toggle counter', Counter.settings);
		return false;
	});

	$('#reset_button').on('click', function() {
		Counter.settings.until = $('#time_input input').val();
		Counter.settings.action = 'pause';
		Counter.socket.emit('reset counter', Counter.settings);
		$('#counter_button').removeClass('button_pause');
		$('#defaultCountdown').removeClass('counter_done');
		$('#counter_button').addClass('button_resume');
		$('#counter_button').addClass('start');
		$('#counter_button').text('Start');
		return false;
	});

	$('#message_button').on('click', function() {
		Counter.socket.emit('send message', $('#message_input').val());
		return false;
	});
	$('#clear_button').on('click', function() {
		$('#message_input').val('');
		Counter.socket.emit('send message', $('#message_input').val());
		return false;
	});
});